# Workshop Planner PWA

This is project of single-page, progressive web app of Workshop planner tools.

Access full documentation :

- [Stable online documentation](https://savadenn-public.gitlab.io/workshop-tools/workshop-pwa/)
- [Documentation source](./docs)
- Build `dist/docs/Workshop.pdf` locally
  ```
  make build.doc
  ```

## Contact

You may join us on our public [Matrix room](https://matrix.to/#/#workshop-tools:savadenn.ems.host)

## Authors and contributors

Time Manager is made by [Savadenn](https://www.savadenn.bzh) with the help of individual contributors.

The complete list can be found [here](https://gitlab.com/savadenn-public/workshop-tools/workshop-pwa/-/graphs/latest).
