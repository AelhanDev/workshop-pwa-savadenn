# Folders hierarchy

## Project {#project-tree}

| File / Folder | Usage                                    |
| :------------ | :--------------------------------------- |
| docs          | Project documentation                    |
| src           | See `::sources-tree`                     |
| tests         | See `::tests-tree`                       |
| public        | Public files loaded at root of webserver |

::: note
**Generated directories**

| File / Folder   | Usage                                  |
| :-------------- | :------------------------------------- |
| `coverage/`     | Code coverage reports                  |
| `dist/`         | Build output                           |
| `node_modules/` | JS dependencies                        |
| `junit.xml`     | Unit test report                       |
| `*.g.*`         | File marker for script generated files |

:::

## Sources {#sources-tree}

::: tip

Source folder `./src` can be referred as `@` inside import.
Ex : `import { Footer } from '@/components'`{.js}

:::

| File / Folder      | Usage                                                                                                  |
| :----------------- | :----------------------------------------------------------------------------------------------------- |
| `src/api`          | Resources & Clients for backend API                                                                    |
| `src/components/`  | See `::components`                                                                                     |
| `src/lang/`        | Contains translations for supported languages                                                          |
| `src/pages/`       | All pages                                                                                              |
| `src/plugins/`     | `::vue` Plugins ([VueRouter](https://next.router.vuejs.org/), [VueI18n](https://vue-i18n.intlify.dev)) |
| `src/states/`      | See `::states`                                                                                         |
| `src/style/`       | See `::style`                                                                                          |
| `src/types/`       | See `::types`                                                                                          |
| `src/AppError.vue` | Application loaded if an error occur during vue initialization                                         |
| `src/App.vue`      | Main vue App                                                                                           |
| `src/main.ts`      | Entrypoint where `::vue` App is created                                                                |
| `src/PrimeVue.ts`  | Loader from `::prime`                                                                                  |

## Tests hierarchy {#tests-tree}

| File / Folder   | Usage                           |
| :-------------- | :------------------------------ |
| `tests/e2e/`    | End-to-end testing with Cypress |
| `**/__tests__/` | Unit testing with Jest          |
