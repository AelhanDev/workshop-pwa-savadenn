# Technical stack

## Main technologies

:::{def-for=vue}
This project uses [`Vue`{#vue}](https://v3.vuejs.org/) as the main framework.
:::

:::{def-for=vite}
It is the bundle with [`Vite`{#vite}](https://vitejs.dev/) to create a reactive dev environment as well as robust production bundle.
:::

:::{def-for=ts}
Sources are written in [`Typescript`{#ts}](https://www.typescriptlang.org/)
:::

The whole dev stack use docker & docker-compose

## Coding standards

::: tip
Check style and reformat code with `make lint`
:::

:::{def-for=eslint}
Code quality is checked by [`ESlint`{#eslint}](https://eslint.org/).
:::

:::{def-for=prettier}
Code style is verified by [`Prettier`{#prettier}](https://prettier.io/).
:::

:::{def-for=stylelint}
Style code style is verified by [`Stylelint`{#stylelint}](https://stylelint.io/).
:::

## Testings

::::::{def-for=jest}
Unit tests are processed by [`Jest`{#jest}](https://jestjs.io/) with the necessary plugins to process `::vue` and `::ts`.

:::tip
Run tests with :

- `make test.unit`
- `make test.coverage` (with sweet code coverage)

:::
::::::

::::::{def-for=e2e}
Complete your tests with [`Cypress`{#e2e}](https://www.cypress.io/) for awesome E2E testing.

::: tip

- make test.e2e
- make test.e2e.interactive

:::
::::::

## Web components

:::{def-for=prime}
The principal web component library is [`PrimeVue`{#prime}](https://primefaces.org/primevue/showcase).
:::

Icons come preferably from [MaterialDesignIcons](https://materialdesignicons.com/).
