# Development

The main providers of tools is `make` command (provided by the `Makefile`). `make` to list the available commands.

## Getting started

- Clone project
- Run
  ```shell
  make clean
  make install
  make dev
  ```
