import PrimeVue, { usePrimeVue } from 'primevue/config'
import { computed, createApp } from 'vue'
import { settings } from '@/states'
import * as PrimeTranslations from '@/lang/prime'
import 'quill'

import Menubar from 'primevue/menubar/Menubar.vue'
import Sidebar from 'primevue/sidebar/Sidebar.vue'
import Button from 'primevue/button/Button.vue'
import Menu from 'primevue/menu/Menu.vue'
import Dropdown from 'primevue/dropdown/Dropdown.vue'
import Calendar from 'primevue/calendar/Calendar.vue'
import SelectButton from 'primevue/selectbutton/SelectButton.vue'
import Card from 'primevue/card/Card.vue'
import Toast from 'primevue/toast/Toast.vue'
import TabMenu from 'primevue/tabmenu/TabMenu.vue'
import Dialog from 'primevue/dialog/Dialog.vue'
import Paginator from 'primevue/paginator/Paginator.vue'
import Password from 'primevue/password/Password.vue'
import InputText from 'primevue/inputtext/InputText.vue'
import InputSwitch from 'primevue/inputswitch/InputSwitch.vue'
import ProgressSpinner from 'primevue/progressspinner/ProgressSpinner.vue'
import PanelMenu from 'primevue/panelmenu/PanelMenu.vue'
import DataTable from 'primevue/datatable/DataTable.vue'
import Toolbar from 'primevue/toolbar/Toolbar.vue'
import Textarea from 'primevue/textarea/Textarea.vue'
import Column from 'primevue/column/Column.vue'
import Editor from 'primevue/editor/Editor.vue'
import InputNumber from 'primevue/inputnumber/InputNumber.vue'
import AutoComplete from 'primevue/autocomplete/AutoComplete.vue'
import Skeleton from 'primevue/skeleton/Skeleton.vue'
import Avatar from 'primevue/avatar/Avatar.vue'
import AvatarGroup from 'primevue/avatargroup/AvatarGroup.vue'
import Breadcrumb from 'primevue/breadcrumb/Breadcrumb.vue'
import Tooltip from 'primevue/tooltip'
import ToastService from 'primevue/toastservice'
import MultiSelect from 'primevue/multiselect/MultiSelect.vue'

/**
 * Register all needed PrimeVue Component
 */
export function registerPrimeVue(app: ReturnType<typeof createApp>): void {
  // Load PrimeVue
  app.use(PrimeVue, {
    ripple: true,
    inputStyle: 'filled',
    locale: computed(
      () =>
        (
          PrimeTranslations as Record<
            string,
            ReturnType<typeof usePrimeVue>['config']['locale']
          >
        )[settings.lang] || PrimeTranslations.en
    ),
  })

  // Register PrimeVue Component
  app.component('PAutoComplete', AutoComplete)
  app.component('PAvatar', Avatar)
  app.component('PAvatarGroup', AvatarGroup)
  app.component('PButton', Button)
  app.component('PCalendar', Calendar)
  app.component('PCard', Card)
  app.component('PColumn', Column)
  app.component('PDialog', Dialog)
  app.component('PDropdown', Dropdown)
  app.component('PMultiSelect', MultiSelect)
  app.component('PEditor', Editor)
  app.component('PInputText', InputText)
  app.component('PInputNumber', InputNumber)
  app.component('PInputSwitch', InputSwitch)
  app.component('PMenu', Menu)
  app.component('PMenubar', Menubar)
  app.component('PDataTable', DataTable)
  app.component('PPaginator', Paginator)
  app.component('PPassword', Password)
  app.component('PPanelMenu', PanelMenu)
  app.component('PProgressSpinner', ProgressSpinner)
  app.component('PSelectButton', SelectButton)
  app.component('PSidebar', Sidebar)
  app.component('PSkeleton', Skeleton)
  app.component('PTabMenu', TabMenu)
  app.component('PTextarea', Textarea)
  app.component('PToast', Toast)
  app.component('PToolbar', Toolbar)
  app.component('PBreadcrumb', Breadcrumb)

  app.directive('tooltip', Tooltip)

  app.directive('focus', {
    // When the bound element is mounted into the DOM...
    mounted(el) {
      // Focus the element
      el.focus()
    },
  })

  app.use(ToastService)
}
