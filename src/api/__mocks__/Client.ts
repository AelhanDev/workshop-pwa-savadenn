/**
 * @see https://jestjs.io/docs/manual-mocks#mocking-user-modules
 */

import axios, { AxiosInstance } from 'axios'

/**
 * Returns secure API client
 */
export async function getApiClient(): Promise<AxiosInstance> {
  return axios
}

/**
 * Returns API client for unauthenticated calls
 */
export async function getApiClientPublic(): Promise<AxiosInstance> {
  return axios
}
