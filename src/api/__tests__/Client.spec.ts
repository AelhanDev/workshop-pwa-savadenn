import axios from 'axios'
import MockAdapter from 'axios-mock-adapter'
import * as Security from '@/api/security'
import { createApiClient, getApiClient, getApiClientPublic } from '@/api/Client'
import { mocked } from 'ts-jest/utils'

jest.mock('@/api/security')

const mockAxios = new MockAdapter(axios, { onNoMatch: 'throwException' })
const mockedSecurity = mocked(Security)

let axiosInstance = axios.create()
let mockAxiosInstance = new MockAdapter(axiosInstance, {
  onNoMatch: 'throwException',
})

beforeEach(() => {
  mockAxios.reset()
  axiosInstance = axios.create()
  mockAxiosInstance = new MockAdapter(axiosInstance, {
    onNoMatch: 'throwException',
  })
})

describe('Client.ts', () => {
  it('creates only one instance of client', async () => {
    const secure1 = await getApiClient()
    const secure2 = await getApiClient()
    expect(secure1).toBe(secure2)

    const public1 = await getApiClientPublic()
    const public2 = await getApiClientPublic()
    expect(public1).toBe(public2)
  })

  it('creates client with given instance', async () => {
    expect(await createApiClient(true, axiosInstance)).toBe(axiosInstance)
    expect(await createApiClient(false, axiosInstance)).toBe(axiosInstance)
    expect(await createApiClient(true)).not.toBe(axiosInstance)
    expect(await createApiClient(false)).not.toBe(axiosInstance)
  })

  it('add headers on secure request', async () => {
    mockAxiosInstance.onAny().reply(200)
    mockedSecurity.getToken.mockImplementation(async () => 'mytoken')
    const client = await createApiClient(true, axiosInstance)

    await client.get('/anything')
    const mockGet = mockAxiosInstance.history.get
    expect(mockGet.length).toBe(1)
    expect(mockGet[0].headers?.Authorization).toEqual('Bearer mytoken')
    expect(mockGet[0].headers?.['Content-Type']).toBe(undefined)

    await client.post('/anything')
    const mockPost = mockAxiosInstance.history.post
    expect(mockPost.length).toBe(1)
    expect(mockPost[0].headers?.Authorization).toEqual('Bearer mytoken')
    expect(mockPost[0].headers?.['Content-Type']).toBe('application/json')

    await client.patch('/anything')
    const mockPatch = mockAxiosInstance.history.patch
    expect(mockPatch.length).toBe(1)
    expect(mockPatch[0].headers?.Authorization).toEqual('Bearer mytoken')
    expect(mockPatch[0].headers?.['Content-Type']).toBe(
      'application/merge-patch+json'
    )
  })

  it('add headers on public request', async () => {
    mockAxiosInstance.onAny().reply(200)
    mockedSecurity.getToken.mockImplementation(async () => null)

    const client = await createApiClient(false, axiosInstance)
    await client.get('/anything')

    const mockGet = mockAxiosInstance.history.get
    expect(mockGet.length).toBe(1)
    expect(mockGet[0].headers?.Authorization).toBeUndefined()
  })

  it('reject secure request if no token is provided', async () => {
    mockAxiosInstance.onAny().reply(200)
    mockedSecurity.getToken.mockImplementation(async () => null)

    const client = await createApiClient(true, axiosInstance)

    await expect(client.get('/anything')).rejects.toBe(
      'No bearer token, aborting api call'
    )

    expect(mockAxiosInstance.history.get.length).toBe(0)
  })

  it('refresh token on 401 and replay request', async () => {
    mockAxiosInstance
      .onGet('/anything')
      .replyOnce(401)
      .onGet('/anything')
      .replyOnce(200)
    mockedSecurity.getToken.mockImplementation(async () => 'my token')
    mockedSecurity.refreshTokens.mockImplementation(async () => true)

    const client = await createApiClient(true, axiosInstance)

    await expect(client.get('/anything')).resolves.toHaveProperty('status', 200)

    expect(mockAxiosInstance.history.get.length).toBe(2)
  })
})
