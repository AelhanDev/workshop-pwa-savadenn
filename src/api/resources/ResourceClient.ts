import type { HydraResource, Resource, ResourceList } from '.'
import { getApiClient, removeEmpty } from '@/api'

export type CollectionParam = { page?: number; itemsPerPage?: number }

export type PartialWithoutId<T> = {
  [P in Exclude<keyof T, 'id'>]?: T[P]
}

export type ResourceClient<
  R extends Resource = Resource,
  CP extends CollectionParam = CollectionParam
> = {
  /**
   * Get one resource from its id
   */
  getItem?: (id: R['id']) => Promise<R>

  /**
   * Search for multiple resources
   */
  getCollection?: (params: CP) => Promise<ResourceList<R>>

  /**
   * Creates a new resource
   */
  create?: (resource: PartialWithoutId<R>) => Promise<R>

  /**
   * Partially updates current resource
   */
  patch?: (id: R['id'], model: PartialWithoutId<R>) => Promise<R>

  /**
   * Deletes the resource
   */
  delete?: (id: R['id']) => Promise<void>
}

/**
 * Generic implementation of ResourceClient
 */
export abstract class AbstractResourceClient<
  R extends Resource,
  CP extends CollectionParam = CollectionParam
> implements ResourceClient<R, CP>
{
  /**
   * Resource endpoint
   */
  readonly endpoint: string

  /**
   * @inheritDoc
   */
  constructor(endpoint: string) {
    this.endpoint = endpoint
  }

  /**
   * @inheritDoc
   */
  async delete(uuid: R['id']): Promise<void> {
    const client = await getApiClient()
    await client.delete(uuid)
  }

  /**
   * @inheritDoc
   */
  async getItem(uuid: R['id']): Promise<R> {
    const client = await getApiClient()
    const { data } = await client.get(uuid)
    return this.cast(data)
  }

  /**
   * @inheritDoc
   */
  async getCollection(params: CP): Promise<ResourceList<R>> {
    const client = await getApiClient()

    if (!params.page) params.page = 1
    const { data } = await client.get(this.endpoint, {
      params: removeEmpty(params),
    })

    return {
      resources: data['hydra:member'].map(this.cast) ?? [],
      total: data['hydra:totalItems'] ?? 0,
      next: data?.['hydra:view']?.['hydra:next']
        ? async () =>
            await this.getCollection({
              ...params,
              page: (params.page || 1) + 1,
            })
        : undefined,
    }
  }

  /**
   * @inheritDoc
   */
  async patch(id: R['id'], model: PartialWithoutId<R>): Promise<R> {
    const client = await getApiClient()
    const { data } = await client.patch(id, model)
    return this.cast(data)
  }

  /**
   * @inheritDoc
   */
  async create(resource: PartialWithoutId<R>): Promise<R> {
    const client = await getApiClient()
    const { data } = await client.post(this.endpoint, resource)
    return this.cast(data)
  }

  /**
   * Casts API return object into APP object
   */
  protected abstract cast(obj: R & HydraResource): R
}
