import axios from 'axios'
import MockAdapter from 'axios-mock-adapter'
import {
  BusinessCasesParams,
  businessCaseClient,
  BusinessCaseClient,
} from '@/api/resources'

const mockAxios = new MockAdapter(axios, { onNoMatch: 'throwException' })
jest.mock('@/api/Client')

describe('BusinessCaseClient', () => {
  test('BusinessCaseClient.getCollection', async () => {
    mockAxios.onGet('/api/business_cases').reply(200, {
      'hydra:member': [{ '@id': 1 }, { '@id': 2 }, { '@id': 3 }],
      'hydra:totalItems': 3,
    })

    const actual = await businessCaseClient.getCollection({})
    const expected = {
      resources: [
        { id: 1, dueDate: null },
        { id: 2, dueDate: null },
        { id: 3, dueDate: null },
      ],
      total: 3,
    }

    expect(actual).toEqual(expected)
  })

  test('BusinessCaseClient.getItem', async () => {
    mockAxios.onGet('/api/business_cases/foo-uuid').reply(200, {
      '@id': '/api/business_cases/foo-uuid',
      name: 'A business case',
      code: '666',
      description: 'Foo description',
    })

    const actual = await businessCaseClient.getItem(
      '/api/business_cases/foo-uuid'
    )

    const expected = {
      id: '/api/business_cases/foo-uuid',
      name: 'A business case',
      code: '666',
      description: 'Foo description',
      dueDate: null,
    }
    expect(actual).toEqual(expected)
  })

  test('BusinessCaseClient.create', async () => {
    const requestBody = {
      code: '666',
      name: 'Something',
      description: 'A description',
    }

    mockAxios.onPost('/api/business_cases', requestBody).reply(201, {
      '@id': 'new-uuid',
      code: '666',
      name: 'Something',
      description: 'A description',
    })

    const actual = await businessCaseClient.create(requestBody)

    const expected = {
      id: 'new-uuid',
      code: '666',
      name: 'Something',
      description: 'A description',
      dueDate: null,
    }
    expect(actual).toEqual(expected)
  })

  test('BusinessCaseClient.patch', async () => {
    const requestBody = {
      id: '/api/business_cases/uuid',
      description: 'New description',
    }
    mockAxios.onPatch('/api/business_cases/uuid', requestBody).reply(200, {
      '@id': '/api/business_cases/foo-uuid',
      name: 'Task',
      code: '666',
      description: 'New description',
    })

    const actual = await businessCaseClient.patch(
      '/api/business_cases/uuid',
      requestBody
    )

    expect(actual).toEqual({
      code: '666',
      createdAt: undefined,
      description: 'New description',
      id: '/api/business_cases/foo-uuid',
      dueDate: null,
      name: 'Task',
    })
  })

  describe('BusinessClient.paramsToQueryParams', () => {
    test('test with dueDateBefore filter', () => {
      const now = new Date()
      const params: BusinessCasesParams = {
        page: 42,
        code: 'TX42',
        name: 'A name',
        dueDateBefore: now,
        'order[dueDate]': 'DESC',
      }
      const actual = BusinessCaseClient.paramsToQueryParams(params)

      const expected = {
        page: '42',
        code: 'TX42',
        name: 'A name',
        'dueDate[before]': now.toISOString(),
        'order[dueDate]': 'DESC',
      }

      expect(actual).toEqual(expected)
    })

    test('test with dueDateAfter filter', () => {
      const now = new Date()
      const params: BusinessCasesParams = {
        page: 12,
        code: 'CS26',
        name: 'Another',
        dueDateAfter: now,
        'order[dueDate]': 'ASC',
      }
      const actual = BusinessCaseClient.paramsToQueryParams(params)

      const expected = {
        page: '12',
        code: 'CS26',
        name: 'Another',
        'dueDate[after]': now.toISOString(),
        'order[dueDate]': 'ASC',
      }

      expect(actual).toEqual(expected)
    })
  })
})
