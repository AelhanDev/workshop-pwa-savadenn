import axios from 'axios'
import MockAdapter from 'axios-mock-adapter'
import { controlPointClient } from '@/api/resources/controlPoint'

const mockAxios = new MockAdapter(axios, { onNoMatch: 'throwException' })
jest.mock('@/api/Client')

describe('ControlPointClient', () => {
  test('ControlPointClient.getCollection', async () => {
    mockAxios.onGet('/api/control_points').reply(200, {
      'hydra:member': [
        {
          '@id': 1,
          description: 'Dummy description',
          createdAt: '2022-01-01 10:28:00',
          task: '/api/tasks/t-uuid',
          controlPointValidations: [],
        },
      ],
      'hydra:totalItems': 1,
    })

    const actual = await controlPointClient.getCollection({})
    const expected = {
      resources: [
        {
          id: 1,
          description: 'Dummy description',
          createdAt: new Date('2022-01-01 10:28:00'),
          task: '/api/tasks/t-uuid',
          controlPointValidations: [],
        },
      ],
      total: 1,
    }

    expect(actual).toEqual(expected)
  })
})
