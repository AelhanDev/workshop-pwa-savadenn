import { getParentId, getType, ResourceType } from '..'

describe('getParentId', () => {
  test('task', () => {
    expect(
      getParentId({
        id: '/api/tasks/5c93cc2e-4bfa-4263-a247-538f2035ea0a',
        businessCase:
          '/api/business_cases/2a50cf0f-ff8b-40ab-bec7-45d9a1c8f98e',
      })
    ).toBe('/api/business_cases/2a50cf0f-ff8b-40ab-bec7-45d9a1c8f98e')
  })

  test('no parent', () => {
    expect(getParentId({ id: '/auth/users/uuid' })).toBeUndefined()
  })
  test('unknown resource', () => {
    expect(getParentId({ id: '/uuid' })).toBeUndefined()
  })
})
describe('getType', () => {
  test('task', () => {
    expect(
      getType({
        id: '/api/tasks/002ea81b-7e51-4eb9-b76b-46fed03fc3a9',
        businessCase:
          '/api/business_cases/7b1df50e-af47-4db4-b8ea-c587e8926178',
      })
    ).toBe(ResourceType.Task)
  })

  test('businessCase', () => {
    expect(
      getType({
        id: '/api/business_cases/7b1df50e-af47-4db4-b8ea-c587e8926178',
      })
    ).toBe(ResourceType.BusinessCase)
  })

  test('unknown resource', () => {
    expect(getType({ id: '/uuid' })).toBeUndefined()
  })
})
