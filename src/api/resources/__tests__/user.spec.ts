import axios from 'axios'
import MockAdapter from 'axios-mock-adapter'
import { userClient } from '@/api/resources'

const mockAxios = new MockAdapter(axios, { onNoMatch: 'throwException' })
jest.mock('@/api/Client')

describe('UserClient', () => {
  it('UserClient.getItem', async () => {
    mockAxios.onGet('/auth/user/uuid').reply(200, {
      '@id': '/auth/user/uuid',
      email: 'my@mail.example',
    })

    const actual = await userClient.getItem('/auth/user/uuid')

    expect(actual).toEqual({
      id: '/auth/user/uuid',
      name: '',
      email: 'my@mail.example',
      roles: [],
    })
  })

  it('UserClient.patch', async () => {
    const requestBody = {
      id: '/auth/user/uuid',
      name: 'My New Name',
    }

    mockAxios.onPatch('/auth/user/uuid', requestBody).reply(200, {
      '@id': '/auth/user/uuid',
      name: 'My New Name',
      email: 'my@mail.example',
      roles: ['MY_ROLE'],
    })

    const actual = await userClient.patch('/auth/user/uuid', requestBody)

    expect(actual).toEqual({
      id: '/auth/user/uuid',
      name: 'My New Name',
      email: 'my@mail.example',
      roles: ['MY_ROLE'],
    })
  })

  it('UserClient.consumeConfirmEmailToken', async () => {
    mockAxios
      .onPost('/auth/confirm_email', {
        token: 'foo-token',
      })
      .reply(200)

    await expect(
      userClient.consumeConfirmEmailToken('foo-token')
    ).resolves.toBe(undefined)
  })
})
