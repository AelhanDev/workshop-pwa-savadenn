import axios from 'axios'
import MockAdapter from 'axios-mock-adapter'
import { workforceClient } from '@/api/resources'

const mockAxios = new MockAdapter(axios, { onNoMatch: 'throwException' })
jest.mock('@/api/Client')

describe('WorkforceClient', () => {
  test('WorkforceClient.getCollection', async () => {
    mockAxios.onGet('/api/workforces').reply(200, {
      'hydra:member': [
        { '@id': 1, name: 'toto' },
        { '@id': 2, name: 'truc' },
        { '@id': 3, name: 'foo' },
      ],
      'hydra:totalItems': 3,
    })

    const actual = await workforceClient.getCollection({ page: 1 })
    const expected = {
      resources: [
        { id: 1, name: 'toto' },
        { id: 2, name: 'truc' },
        { id: 3, name: 'foo' },
      ],
      total: 3,
    }

    expect(actual).toEqual(expected)
  })

  test('WorkforceClient.getItem', async () => {
    mockAxios
      .onGet('/api/workforces/1')
      .reply(200, { '@id': 1, name: 'toto' }, { '@id': 2, name: 'truc' })

    const actual = await workforceClient.getItem('/api/workforces/1')

    expect(actual).toEqual({ id: 1, name: 'toto' })
  })
})
