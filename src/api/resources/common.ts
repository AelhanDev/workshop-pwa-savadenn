export type Resource = { id: string }
export type HydraResource = { '@id': string }

export type PartialResource<T extends Resource> = Partial<T> & {
  id: T['id']
}

export const PER_PAGE_COUNT = 30

/**
 * List of a specific resource
 */
export type ResourceList<T extends Resource> = {
  resources: T[]

  /**
   * Total resource, if total > values.length, there are more pages
   */
  total: number

  /**
   * If present, call this to query next resources
   */
  next?: () => Promise<ResourceList<T>>
}

/**
 * Resources
 */
export type ResourceMap<T extends Resource> = Map<T['id'], T>

/**
 * Creates a copy of source object without empty fields
 */
export function removeEmpty(
  source?: Record<string, unknown> | null
): Record<string, unknown> {
  if (!source) return {}
  return Object.fromEntries(
    Object.entries(source).filter(
      ([_, v]) => v !== undefined && v !== null && v !== ''
    )
  )
}
