import {
  AbstractResourceClient,
  CollectionParam,
  HydraResource,
  User,
} from '@/api'
import { ControlPoint } from '@/api/resources/controlPoint'

export type ControlPointValidationParams = CollectionParam & {
  controlPoint?: ControlPoint['id']
}

export type ControlPointValidation = {
  id: string
  verifier: User['id']
  createdAt: Date
  controlPoint: ControlPoint['id']
}

export class ControlPointValidationClient extends AbstractResourceClient<
  ControlPointValidation,
  ControlPointValidationParams
> {
  protected cast(
    obj: ControlPointValidation & HydraResource
  ): ControlPointValidation {
    return {
      id: obj['@id'],
      verifier: obj.verifier,
      createdAt: new Date(obj.createdAt),
      controlPoint: obj.controlPoint,
    }
  }
}
export const controlPointValidationClient = new ControlPointValidationClient(
  '/api/control_point_validations'
)
