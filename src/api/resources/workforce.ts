import { HydraResource, AbstractResourceClient, PartialWithoutId } from '.'

export type Workforce = {
  id: string
  name: string
}

class WorkforceClient extends AbstractResourceClient<Workforce> {
  async create(resource: PartialWithoutId<Workforce>): Promise<Workforce> {
    throw new Error('Cannot create workforce, either create Teams or Machines')
  }

  /**
   * Returns qualified Workforce
   */
  cast(obj: Workforce & HydraResource): Workforce {
    return {
      id: obj['@id'],
      name: obj.name,
    }
  }
}

export const workforceClient = new WorkforceClient('/api/workforces')
