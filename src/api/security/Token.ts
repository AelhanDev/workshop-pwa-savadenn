import { settings } from '@/states'
import jwtDecode from 'jwt-decode'
import { JwtPayload } from '@/types'
import { refreshTokens } from '@/api/security'

const KEY = 'token'
const MINIMUM_VALIDITY_REMAINING = 60

/**
 * Get token for API call
 */
export async function getToken(): Promise<string | null> {
  let token = sessionStorage.getItem(KEY)

  const renew = async () => {
    await refreshTokens()
    token = sessionStorage.getItem(KEY)
  }

  // Verify token exists
  if (!token) await renew()
  if (!token) return null

  // Verify token is valid
  let payload: JwtPayload | null
  try {
    payload = jwtDecode<JwtPayload>(token)
  } catch (e) {
    await renew()
    payload = jwtDecode<JwtPayload>(token)
  }

  // Verify token is not close to expiration
  if (
    payload.exp &&
    payload.exp - Date.now() / 1000 < MINIMUM_VALIDITY_REMAINING
  )
    await renew()

  return token
}

/**
 * Save token for API call
 * @param jwt
 */
export function setToken(jwt: string | null): void {
  if (jwt) {
    sessionStorage.setItem(KEY, jwt)
    try {
      const payload = jwtDecode<JwtPayload>(jwt)
      settings.user.id = payload.guid.includes('users')
        ? `${payload.guid}`
        : `/auth/users/${payload.guid}`
      return
    } catch (e) {
      console.error('Invalid JWT')
    }
  }
  sessionStorage.removeItem(KEY)
}
