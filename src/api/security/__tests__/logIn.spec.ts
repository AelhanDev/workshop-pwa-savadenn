import axios from 'axios'
import MockAdapter from 'axios-mock-adapter'
import { logIn, LOGIN_ENDPOINT } from '..'
import { resetSettings, settings } from '@/states'
import { encode } from 'jwt-simple'

const mockAxios = new MockAdapter(axios, { onNoMatch: 'throwException' })

beforeEach(() => {
  mockAxios.reset()
  resetSettings()
})

const credentials = {
  email: 'rick@multiverse.example',
  password: 'PortalGun',
}

describe('logIn', () => {
  it('Logs with valid credentials', async () => {
    mockAxios.onPost(LOGIN_ENDPOINT).reply(200, {
      token: encode({ guid: '1' }, 'my-secret'),
      refresh_token: 'refresh',
    })

    await expect(logIn(credentials)).resolves.toBe(true)
    expect(settings.refreshToken).toBe('refresh')
    expect(settings.user.id).toBe('/auth/users/1')
  })

  it('returns false if credentials are wrong', async () => {
    mockAxios.onPost(LOGIN_ENDPOINT).reply(401)
    await expect(logIn(credentials)).resolves.toBe(false)
  })

  it('throw otherwise', async () => {
    mockAxios.onPost(LOGIN_ENDPOINT).reply(404)

    await expect(logIn(credentials)).rejects.toEqual(
      new Error('Request failed with status code 404')
    )
  })
})
