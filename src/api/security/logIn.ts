import { settings } from '@/states'
import { setToken } from './Token'
import { getApiClientPublic } from '@/api'
import { AxiosError } from 'axios'

export const LOGIN_ENDPOINT = '/auth/authentication_token'

/**
 * Log in an retrieves API tokens
 * @param params
 */
export async function logIn(params: {
  email: string
  password: string
}): Promise<boolean> {
  try {
    const client = await getApiClientPublic()
    const { data } = await client.post(LOGIN_ENDPOINT, {
      email: params.email,
      password: params.password,
    })

    setToken(data.token)
    settings.refreshToken = data.refresh_token
    return true
  } catch (e) {
    if ((e as AxiosError)?.response?.status === 401) return false
    throw e
  }
}
