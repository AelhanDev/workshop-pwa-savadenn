import { settings } from '@/states'
import { setToken } from './Token'
import { getApiClientPublic, logOut } from '@/api'

let renewTokenPromise: Promise<boolean> | null

const REFRESH_URL = '/auth/refresh_token'

/**
 * Refresh API token with the refresh token
 */
export async function run(): Promise<boolean> {
  // Create a new client to have no interceptors (prevent looping on 401)
  const client = await getApiClientPublic()

  if (settings.refreshToken) {
    try {
      const { data } = await client.post(
        REFRESH_URL,
        { refresh_token: settings.refreshToken },
        { validateStatus: (status) => status < 400 }
      )
      setToken(data.token)
      settings.refreshToken = data.refresh_token
      return true
    } catch (exception) {
      console.error(exception)
    }
  }
  await logOut(true)
  return false
}

/**
 * Gets new tokens via the refresh route
 */
export async function refreshTokens(): Promise<boolean> {
  if (!renewTokenPromise) {
    renewTokenPromise = run()
    renewTokenPromise.then(() => (renewTokenPromise = null))
  }
  return renewTokenPromise
}
