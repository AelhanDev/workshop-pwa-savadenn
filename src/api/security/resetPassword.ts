import { getApiClientPublic } from '@/api'
import { settings } from '@/states'
import { setToken } from './Token'
import { AxiosError } from 'axios'

const RESET_ENDPOINT = '/auth/reset_password'

/**
 * Initiates access recovery flow
 * @param params
 */
export async function initiateResetPassword(params: {
  email: string
}): Promise<true> {
  const client = await getApiClientPublic()
  await client.post(RESET_ENDPOINT, params)
  return true
}

/**
 * Reset password
 * @param params
 */
export async function resetPassword(params: {
  password: string
  token: string
}): Promise<boolean> {
  try {
    const client = await getApiClientPublic()
    const result = await client.post(`/auth/reset_password/${params.token}`, {
      password: params.password,
    })
    setToken(result.data.token)
    settings.refreshToken = result.data.refresh_token
    return true
  } catch (e) {
    if ((e as AxiosError)?.response?.status === 400) return false
    if ((e as AxiosError)?.response?.status === 422) return false
    throw e
  }
}
