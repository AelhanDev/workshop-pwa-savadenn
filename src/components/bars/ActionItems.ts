import { logOut } from '@/api'
import { computed, readonly } from 'vue'
import { settings } from '@/states'
import { ActionItem } from '@/components/bars/MenuItem'

export const LogOut: ActionItem = readonly({
  class: 'Menu__LogOut p-button-danger',
  label: 'component.menu.logout',
  command: () => logOut(),
  icon: 'mdi mdi-logout',
})

export const ToggleMenu: ActionItem = readonly({
  class: 'Menu__Toggle',
  label: computed(
    () => `component.sidebar.${settings.miniMenu ? 'open' : 'minimize'}`
  ),
  command: () => (settings.miniMenu = !settings.miniMenu),
  icon: computed(
    () => `mdi mdi-chevron-double-${settings.miniMenu ? 'right' : 'left'}`
  ),
})
