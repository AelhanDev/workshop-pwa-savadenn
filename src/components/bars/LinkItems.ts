import { computed } from 'vue'
import { ROUTES } from '@/plugins'
import { LinkItem } from '@/components/bars/MenuItem'

export const Home: LinkItem = {
  class: 'Menu__Home',
  label: 'page.home.title',
  icon: 'mdi mdi-home',
  to: ROUTES.HOME,
}

export const Settings: LinkItem = {
  class: 'Menu__Settings',
  label: 'page.settings.title',
  icon: 'mdi mdi-cog-outline',
  to: ROUTES.SETTINGS,
}

export const BusinessCases: LinkItem = {
  class: 'Menu__BusinessCase',
  label: 'page.business_cases.title',
  icon: 'mdi mdi-handshake-outline',
  to: ROUTES.BUSINESS_CASES,
}

export const WeekPlanning: LinkItem = {
  class: 'Menu__WeekPlanning',
  label: 'page.weekPlanning.title',
  icon: 'mdi mdi-calendar-week',
  to: ROUTES.WEEK_PLANNING,
}

export const Teams: LinkItem = {
  class: 'Menu__Teams',
  label: 'page.teams.title',
  icon: 'mdi mdi-account-multiple-outline',
  to: ROUTES.TEAMS,
}

export const Machines: LinkItem = {
  class: 'Menu__Machines',
  label: 'page.machines.title',
  icon: 'mdi mdi-robot-industrial',
  to: ROUTES.MACHINES,
}

// Common items of both side bars (mobile & desktop)
export const mainMenuItems = computed(() => {
  return [WeekPlanning, BusinessCases, Teams, Machines]
})
