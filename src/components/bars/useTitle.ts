import { computed, ComputedRef } from 'vue'
import { useRoute } from 'vue-router'
import { useI18n } from 'vue-i18n'

/**
 * Returns reactive title
 */
export function getTitle(): ComputedRef<string> {
  const { t } = useI18n()
  return computed(() => {
    const title = useRoute().name
    return typeof title === 'string' ? t(title) : 'Workshop'
  })
}
