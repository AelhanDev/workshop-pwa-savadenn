import { nextTick, reactive, ref, watch } from 'vue'
import { ReactiveRecorder } from '@/components/form/ReactiveRecorder'
import { WatchStopHandle } from '@vue/runtime-core'

type Test = { foo: string; newKey?: string }

let stopWatcher: WatchStopHandle | undefined
const onWatchMock = jest.fn()

describe('ReactiveRecorder', () => {
  beforeEach(() => {
    onWatchMock.mockReset()
  })

  afterEach(() => {
    if (stopWatcher) stopWatcher()
  })

  /**
   * Testing ReactiveRecoder with a Vue.ref object as target
   */
  describe('on ref', () => {
    let source = ref<Test>({ foo: 'bar' })
    let recorder = new ReactiveRecorder(source)

    beforeEach(() => {
      source = ref<Test>({ foo: 'bar' })
      recorder = new ReactiveRecorder(source)
      stopWatcher = watch(() => recorder.result.value.changes, onWatchMock)
    })

    it('handles changes and rollbacks', async () => {
      source.value.foo = 'bar2'
      await nextTick()
      expect(recorder.result.value.hasChanged).toBe(true)
      expect(recorder.result.value.changes).toEqual({ foo: 'bar2' })

      source.value.foo = 'bar'
      await nextTick()
      expect(recorder.result.value.hasChanged).toBe(false)
      expect(recorder.result.value.changes).toEqual({})

      expect(onWatchMock.mock.calls.length).toBe(2)
      expect(onWatchMock.mock.calls[0][0]).toEqual({ foo: 'bar2' })
      expect(onWatchMock.mock.calls[1][0]).toEqual({})
      expect(recorder.reference).toEqual({ foo: 'bar' })
    })

    it('handles new key', async () => {
      source.value.newKey = 'foo'
      await nextTick()
      expect(recorder.result.value.hasChanged).toBe(true)
      expect(recorder.result.value.changes).toEqual({ newKey: 'foo' })

      expect(onWatchMock.mock.calls.length).toBe(1)
      expect(onWatchMock.mock.calls[0][0]).toEqual({ newKey: 'foo' })
      expect(recorder.result.value.reference).toEqual({ foo: 'bar' })
    })

    it('ReactiveRecorder.reset', async () => {
      source.value.foo = 'bar2'
      await nextTick()
      expect(recorder.result.value.hasChanged).toBe(true)
      expect(recorder.result.value.changes).toEqual({ foo: 'bar2' })

      recorder.reset()
      await nextTick()
      expect(recorder.result.value.hasChanged).toBe(false)
      expect(recorder.result.value.changes).toEqual({})

      source.value.foo = 'bar'
      await nextTick()
      expect(recorder.result.value.hasChanged).toBe(true)
      expect(recorder.result.value.changes).toEqual({ foo: 'bar' })

      expect(onWatchMock.mock.calls.length).toBe(3)
      expect(onWatchMock.mock.calls[0][0]).toEqual({ foo: 'bar2' })
      expect(onWatchMock.mock.calls[1][0]).toEqual({})
      expect(onWatchMock.mock.calls[2][0]).toEqual({ foo: 'bar' })
      expect(recorder.reference).toEqual({ foo: 'bar2' })
    })
  })

  describe('as ref', () => {
    let source = ref<Test>({ foo: 'bar' })
    let recorder = new ReactiveRecorder(source)

    beforeEach(() => {
      source = ref<Test>({ foo: 'bar' })
      recorder = new ReactiveRecorder(source)
      stopWatcher = watch(() => recorder.result.value.changes, onWatchMock)
    })

    it('handles changes and rollbacks', async () => {
      source.value = { foo: 'bar2' }
      await nextTick()
      expect(recorder.result.value.hasChanged).toBe(true)
      expect(recorder.result.value.changes).toEqual({ foo: 'bar2' })

      source.value = { foo: 'bar' }
      await nextTick()
      expect(recorder.result.value.hasChanged).toBe(false)
      expect(recorder.result.value.changes).toEqual({})

      expect(onWatchMock.mock.calls.length).toBe(2)
      expect(onWatchMock.mock.calls[0][0]).toEqual({ foo: 'bar2' })
      expect(onWatchMock.mock.calls[1][0]).toEqual({})
      expect(recorder.reference).toEqual({ foo: 'bar' })
    })

    it('handles new key', async () => {
      source.value = { foo: 'bar', newKey: 'foo' }
      await nextTick()
      expect(recorder.result.value.hasChanged).toBe(true)
      expect(recorder.result.value.changes).toEqual({ newKey: 'foo' })

      expect(onWatchMock.mock.calls.length).toBe(1)
      expect(onWatchMock.mock.calls[0][0]).toEqual({ newKey: 'foo' })
      expect(recorder.reference).toEqual({ foo: 'bar' })
    })

    it('ReactiveRecorder.reset', async () => {
      source.value = { foo: 'bar2' }
      await nextTick()
      expect(recorder.result.value.hasChanged).toBe(true)
      expect(recorder.result.value.changes).toEqual({ foo: 'bar2' })

      recorder.reset()
      await nextTick()
      expect(recorder.result.value.hasChanged).toBe(false)
      expect(recorder.result.value.changes).toEqual({})

      source.value = { foo: 'bar' }
      await nextTick()
      expect(recorder.result.value.hasChanged).toBe(true)
      expect(recorder.result.value.changes).toEqual({ foo: 'bar' })

      expect(onWatchMock.mock.calls.length).toBe(3)
      expect(onWatchMock.mock.calls[0][0]).toEqual({ foo: 'bar2' })
      expect(onWatchMock.mock.calls[1][0]).toEqual({})
      expect(onWatchMock.mock.calls[2][0]).toEqual({ foo: 'bar' })
      expect(recorder.reference).toEqual({ foo: 'bar2' })
    })
  })

  /**
   * Testing ReactiveRecoder with a Vue.reactive object as target
   */
  describe('on reactive', () => {
    let source = reactive<Test>({ foo: 'bar' })
    let recorder = new ReactiveRecorder(() => source)

    beforeEach(() => {
      source = reactive<Test>({ foo: 'bar' })
      recorder = new ReactiveRecorder(() => source)
      stopWatcher = watch(() => recorder.result.value.changes, onWatchMock)
    })

    it('handles changes and rollbacks', async () => {
      source.foo = 'bar2'
      await nextTick()
      expect(recorder.result.value.hasChanged).toBe(true)
      expect(recorder.result.value.changes).toEqual({ foo: 'bar2' })

      source.foo = 'bar'
      await nextTick()
      expect(recorder.result.value.hasChanged).toBe(false)
      expect(recorder.result.value.changes).toEqual({})

      expect(onWatchMock.mock.calls.length).toBe(2)
      expect(onWatchMock.mock.calls[0][0]).toEqual({ foo: 'bar2' })
      expect(onWatchMock.mock.calls[1][0]).toEqual({})
      expect(recorder.reference).toEqual({ foo: 'bar' })
    })

    it('handles new key', async () => {
      source.newKey = 'foo'
      await nextTick()
      expect(recorder.result.value.hasChanged).toBe(true)
      expect(recorder.result.value.changes).toEqual({ newKey: 'foo' })

      expect(onWatchMock.mock.calls.length).toBe(1)
      expect(onWatchMock.mock.calls[0][0]).toEqual({ newKey: 'foo' })
      expect(recorder.reference).toEqual({ foo: 'bar' })
    })

    it('ReactiveRecorder.reset', async () => {
      source.foo = 'bar2'
      await nextTick()
      expect(recorder.result.value.hasChanged).toBe(true)
      expect(recorder.result.value.changes).toEqual({ foo: 'bar2' })

      recorder.reset()
      await nextTick()
      expect(recorder.result.value.hasChanged).toBe(false)
      expect(recorder.result.value.changes).toEqual({})

      source.foo = 'bar'
      await nextTick()
      expect(recorder.result.value.hasChanged).toBe(true)
      expect(recorder.result.value.changes).toEqual({ foo: 'bar' })

      expect(onWatchMock.mock.calls.length).toBe(3)
      expect(onWatchMock.mock.calls[0][0]).toEqual({ foo: 'bar2' })
      expect(onWatchMock.mock.calls[1][0]).toEqual({})
      expect(onWatchMock.mock.calls[2][0]).toEqual({ foo: 'bar' })
      expect(recorder.reference).toEqual({ foo: 'bar2' })
    })
  })

  /**
   * Testing ReactiveRecoder with target is a sub path of a Vue.reactive object
   */
  describe('on sub object of reactive', () => {
    let source = reactive<{ foobar: Test }>({ foobar: { foo: 'bar' } })
    let recorder = new ReactiveRecorder(() => source.foobar)
    beforeEach(() => {
      source = reactive<{ foobar: Test }>({ foobar: { foo: 'bar' } })
      recorder = new ReactiveRecorder(() => source.foobar)
      stopWatcher = watch(() => recorder.result.value.changes, onWatchMock)
    })

    it('handles changes and rollbacks', async () => {
      source.foobar.foo = 'bar2'
      await nextTick()
      expect(recorder.result.value.hasChanged).toBe(true)
      expect(recorder.result.value.changes).toEqual({ foo: 'bar2' })

      source.foobar.foo = 'bar'
      await nextTick()
      expect(recorder.result.value.hasChanged).toBe(false)
      expect(recorder.result.value.changes).toEqual({})

      expect(onWatchMock.mock.calls.length).toBe(2)
      expect(onWatchMock.mock.calls[0][0]).toEqual({ foo: 'bar2' })
      expect(onWatchMock.mock.calls[1][0]).toEqual({})
      expect(recorder.reference).toEqual({ foo: 'bar' })
    })

    it('handles new key', async () => {
      source.foobar.newKey = 'foo'
      await nextTick()
      expect(recorder.result.value.hasChanged).toBe(true)
      expect(recorder.result.value.changes).toEqual({ newKey: 'foo' })

      expect(onWatchMock.mock.calls.length).toBe(1)
      expect(onWatchMock.mock.calls[0][0]).toEqual({ newKey: 'foo' })
      expect(recorder.reference).toEqual({ foo: 'bar' })
    })

    it('ReactiveRecorder.reset', async () => {
      source.foobar.foo = 'bar2'
      await nextTick()
      expect(recorder.result.value.hasChanged).toBe(true)
      expect(recorder.result.value.changes).toEqual({ foo: 'bar2' })

      recorder.reset()
      await nextTick()
      expect(recorder.result.value.hasChanged).toBe(false)
      expect(recorder.result.value.changes).toEqual({})

      source.foobar.foo = 'bar'
      await nextTick()
      expect(recorder.result.value.hasChanged).toBe(true)
      expect(recorder.result.value.changes).toEqual({ foo: 'bar' })

      expect(onWatchMock.mock.calls.length).toBe(3)
      expect(onWatchMock.mock.calls[0][0]).toEqual({ foo: 'bar2' })
      expect(onWatchMock.mock.calls[1][0]).toEqual({})
      expect(onWatchMock.mock.calls[2][0]).toEqual({ foo: 'bar' })
      expect(recorder.reference).toEqual({ foo: 'bar2' })
    })
  })
})
