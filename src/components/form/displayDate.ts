import moment from 'moment-timezone'

export function displayDate(date: Date): string {
  return moment(date).format('LLL')
}
