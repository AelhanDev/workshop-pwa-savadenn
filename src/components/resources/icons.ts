import { ResourceType } from '@/api'

export const RESOURCE_ICONS: Record<ResourceType, string> = {
  [ResourceType.team]: 'mdi mdi-account-multiple-outline',
  [ResourceType.machine]: 'mdi mdi-robot-industrial',
  [ResourceType.workforce]: 'mdi mdi-account-hard-hat',
  [ResourceType.BusinessCase]: 'mdi mdi-handshake-outline',
  [ResourceType.Task]: 'mdi mdi-cards-outline',
  [ResourceType.User]: 'mdi mdi-account-outline',
}
