export * from './icons'

import { createApp } from 'vue'
import BusinessCaseDialog from './BusinessCaseDialog.vue'
import SearchOrCreate from './SearchOrCreate.vue'
import SimpleCreate from './SimpleCreate.vue'
import CopyBusinessCaseDialog from './CopyBusinessCaseDialog.vue'

/**
 * Registers all form components globally
 */
export function registerResourceComponents(
  app: ReturnType<typeof createApp>
): void {
  app.component('BusinessCaseDialog', BusinessCaseDialog)
  app.component('SearchOrCreate', SearchOrCreate)
  app.component('SimpleCreate', SimpleCreate)
  app.component('CopyBusinessCaseDialog', CopyBusinessCaseDialog)
}
