import { useActions } from '@/pages/businessCases/useActions'
import { ref } from 'vue'
import { BusinessCase, PartialResource } from '@/api'

describe('useActions', function () {
  test('displayCloneDialog', function () {
    const t = jest.fn()
    const model = ref<PartialResource<BusinessCase>>({ id: 'foo' })
    const { displayCloneDialog, items } = useActions(model, t)

    expect(displayCloneDialog.value).toEqual(false)
    expect(items.value.length).toEqual(3)
  })
})
