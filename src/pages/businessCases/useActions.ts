import { computed, ref, Ref, shallowRef } from 'vue'
import { VueI18n } from 'vue-i18n'
import { ActionItem, MenuItem, translate } from '@/components/bars/MenuItem'
import { BusinessCase, PartialResource } from '@/api'
import DueDatePicker from '@/pages/businessCases/partial/DueDatePicker.vue'

export type UseActions = {
  displayCloneDialog: Ref<boolean>
  displayConfirmDelete: Ref<boolean>
  items: Ref<Array<MenuItem | ActionItem>>
  componentSpecificAction: Ref<null | typeof DueDatePicker>
}

export function useActions(
  businessCase: Ref<PartialResource<BusinessCase>>,
  t: VueI18n['t']
): UseActions {
  const componentSpecificAction = shallowRef<null | typeof DueDatePicker>(null)
  const displayCloneDialog = ref(false)
  const displayConfirmDelete = ref(false)

  const copyBusinessCase = {
    label: 'action.clone',
    icon: 'mdi mdi-content-copy',
    command: () => (displayCloneDialog.value = true),
    class: '',
  }

  const changeDueDate = {
    label: 'component.dueDatePicker.update',
    icon: 'mdi mdi-calendar',
    command: () => (componentSpecificAction.value = DueDatePicker),
    class: '',
  }

  const setDueDate = {
    label: 'component.dueDatePicker.set',
    icon: 'mdi mdi-calendar',
    command: () => (componentSpecificAction.value = DueDatePicker),
    class: '',
  }

  const deleteAction = {
    label: t('action.delete'),
    icon: 'mdi mdi-delete-outline',
    command: () => (displayConfirmDelete.value = true),
    class: 'p-menuitem-danger',
  }

  return {
    displayCloneDialog,
    componentSpecificAction,
    displayConfirmDelete,
    items: computed(() => {
      const items: ActionItem[] = []

      if (!businessCase.value.dueDate) items.push(setDueDate)
      else items.push(changeDueDate)

      items.push(copyBusinessCase)
      items.push(deleteAction)

      return items.map((item) => translate(item, t))
    }),
  }
}
