import { useTask } from '@/pages/tasks/useTask'
import { ref } from 'vue'
import { Task } from '@/api'

const uuid = ref<Task['id']>('foo-uuid')

describe('useTask', function () {
  test('task initialization', function () {
    const { task } = useTask(uuid, () => true)

    expect(task.value.id).toBe('foo-uuid')
  })

  test('Task spent time', function () {
    const { task, spentTime } = useTask(uuid, () => true)

    expect(spentTime.value).toEqual(null)

    task.value.startDate = new Date('2022-01-01 10:52:42')
    task.value.endDate = new Date('2022-01-02 21:14:02')

    expect(spentTime.value).toEqual(123680)
  })
})
