import { useValidation } from '@/pages/tasks/useValidation'
import { nextTick, ref } from 'vue'
import MockAdapter from 'axios-mock-adapter'
import axios from 'axios'
import { ControlPointValidation } from '@/api/resources/controlPointValidation'
import * as ShortNames from '@/states/ShortNames'
import { mocked } from 'ts-jest/utils'

jest.mock('@/states/ShortNames')
const t = jest.fn((key: string): string => key)
const mockAxios = new MockAdapter(axios, { onNoMatch: 'throwException' })
const mockedShortName = mocked(ShortNames)
jest.mock('@/api/Client')

describe('useValidation', function () {
  test('Test loadValidations reactivity', async function () {
    const id = ref<string>('cp-uuid')
    const { validations } = useValidation(id, t)

    expect(validations.value.length).toEqual(0)

    mockAxios.onGet('/api/control_point_validations').reply(200, {
      'hydra:member': [
        {
          id: 'foo',
          verifier: 'bar',
          createdAt: '2022-01-10 12:52:32',
          controlPoint: 'new-uuid',
        },
      ],
      'hydra:totalItems': 1,
    })

    // Change id expecting validations to be fetched
    id.value = 'new-uuid'

    // Wait for DOM to be updated
    for (let i = 0; i < 4; i++) await nextTick()

    // Expect validation list to be updated
    expect(validations.value.length).toEqual(1)
  })

  test('getValidationLabel', async function () {
    const id = ref<string>('cp-uuid')
    const { getValidationLabel } = useValidation(id, t)
    const validation: ControlPointValidation = {
      id: '/api/control_point_validation/uuid',
      verifier: 'foo',
      createdAt: new Date('2022-01-01 10:32:51'),
      controlPoint: id.value,
    }

    // Expect getShortName() to return "Karlos"
    mockedShortName.getShortName.mockImplementation((id: string) =>
      ref('Karlos')
    )

    const actual = getValidationLabel(validation)

    expect(actual).toEqual('Karlos - January 1, 2022 10:32 AM')
  })
})
