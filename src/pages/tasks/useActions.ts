import { computed, ref, Ref } from 'vue'
import { VueI18n } from 'vue-i18n'
import { ActionItem, MenuItem, translate } from '@/components/bars/MenuItem'
import { PartialResource, Task } from '@/api'

export type UseActions = {
  displayConfirmDelete: Ref<boolean>
  items: Ref<Array<MenuItem | ActionItem>>
}

export function useActions(
  task: Ref<PartialResource<Task>>,
  t: VueI18n['t'],
  patchTask: () => Promise<boolean>
): UseActions {
  const displayConfirmDelete = ref<boolean>(false)

  /**
   * Patch task applying given end date
   */
  async function setTaskEndDate(endDate: Date | null) {
    task.value.endDate = endDate
    await patchTask()
  }

  /**
   * Patch task to close it
   */
  async function closeTask() {
    await setTaskEndDate(new Date())
  }

  /**
   * Patch task to reopen it
   */
  async function reopenTask() {
    await setTaskEndDate(null)
  }

  const deleteTaskAction = {
    label: t('action.delete'),
    icon: 'mdi mdi-delete-outline',
    command: () => (displayConfirmDelete.value = true),
    class: 'p-menuitem-danger',
  }

  const closeTaskAction = {
    label: t('resource.task.close'),
    icon: '',
    command: () => closeTask(),
    class: '',
  }

  const reopenTaskAction = {
    label: t('resource.task.reopen'),
    icon: '',
    command: () => reopenTask(),
    class: '',
  }

  return {
    displayConfirmDelete,
    items: computed(() => {
      const items: ActionItem[] = []

      if (task.value.endDate) items.push(reopenTaskAction)
      else if (task.value.startDate) items.push(closeTaskAction)

      items.push(deleteTaskAction)

      return items.map((item) => translate(item, t))
    }),
  }
}
