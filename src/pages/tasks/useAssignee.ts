import { ref, Ref, watch } from 'vue'
import type { PartialResource, Task, Workforce } from '@/api'
import { workforceClient } from '@/api'
import { WatchStopHandle } from '@vue/runtime-core'

export type UseAssignee = {
  assignee: Ref<Workforce | null>
}

export function useAssignee(
  task: Ref<PartialResource<Task>>,
  patchTask: () => Promise<boolean>
): UseAssignee {
  const assignee = ref<Workforce | null>(null)
  let stop: WatchStopHandle | null = null

  async function getFromTask() {
    if (!task.value.assignedTo) {
      assignee.value = null
      return
    }

    if (task.value.assignedTo !== assignee.value?.id) {
      assignee.value = await workforceClient.getItem(task.value.assignedTo)
    }
  }

  getFromTask().then(watchChange)

  function watchChange() {
    stop = watch(
      () => assignee.value?.id,
      async (assigneeId) => {
        task.value.assignedTo = assigneeId || null
        await patchTask()
        await getFromTask()
      }
    )
  }

  watch(
    () => task.value.assignedTo,
    async () => {
      stop?.()
      await getFromTask()
      watchChange()
    }
  )

  return {
    assignee,
  }
}
