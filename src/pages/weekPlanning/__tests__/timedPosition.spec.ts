import { getMinDurationWidth } from '@/pages/weekPlanning/timedPosition'

const durationProvider = [
  {
    minEndingIdx: 0,
    maxEndingIdx: 0,
    startingIdx: 0,
    expected: '100%',
  },
  {
    minEndingIdx: 10,
    maxEndingIdx: 20,
    startingIdx: 6,
    expected: '28.6%',
  },
  {
    minEndingIdx: 10,
    maxEndingIdx: 10,
    startingIdx: 10,
    expected: '100%',
  },
]

describe.each(durationProvider)('getMinDurationWidth', function (data) {
  test('should return width in percentage', function () {
    const actual = getMinDurationWidth(
      data.minEndingIdx,
      data.maxEndingIdx,
      data.startingIdx
    )

    expect(actual).toBe(data.expected)
  })
})
