/**
 * Regex for parsing search input value
 */
const TERM_REGEX = /^(\s*#(?<code>\S+)\s*)(\s*(?<title>\S+)\s*)?$/

export type TaskSearch = {
  code?: string
  title?: string
}

/**
 * PArse search task term
 * @param term
 */
export function taskSearchParse(term: string): TaskSearch {
  const groups = term.match(TERM_REGEX)?.groups
  return {
    title: groups ? groups.title : term.trim(),
    code: groups?.code,
  }
}
