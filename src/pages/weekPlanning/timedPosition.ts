import { blocks, config, maxDate, minDate } from '@/states/weekPlanning'
import { computed, ComputedRef, Ref } from 'vue'
import { Moment } from 'moment-timezone'
import { draggedObject } from '@/pages/weekPlanning/handleTaskMoveAndResize'

export type Style = {
  top?: string
  left?: string
  display?: string
  width?: string
  '--minDurationWidth'?: string
}
/**
 * Return style positioning depending on element date
 */
export function dateToPosition(element: {
  startDate?: Date | null
  estimatedEndMax?: Date
  estimatedEndMin?: Date
}): Style {
  if (!element.startDate) {
    return { display: 'none' }
  }

  let startingIdx = 0
  let maxEndingIdx = 0
  let minEndingIdx = 0

  blocks.value.every((moment, idx) => {
    const isAfterStart = moment.isAfter(element.startDate, 'minute')
    if (!isAfterStart) startingIdx = idx

    const isAfterEnd = moment.isAfter(
      element.estimatedEndMax ?? element.estimatedEndMin,
      'minute'
    )

    if (!isAfterEnd) maxEndingIdx = idx

    if (!moment.isAfter(element.estimatedEndMin, 'minute')) {
      minEndingIdx = idx
    }

    // Return false if both are past, stopping `every` loop
    return !(isAfterStart && isAfterEnd)
  })

  const style: Style = {
    left: `${startingIdx * config.blockWidth}px`,
  }

  let nbBlock = 1
  if (element.estimatedEndMax) {
    nbBlock = Math.max(1, maxEndingIdx - startingIdx)
  } else if (element.estimatedEndMin) {
    nbBlock = Math.max(1, minEndingIdx - startingIdx)
  }
  style.width = `${nbBlock * config.blockWidth}px`

  style['--minDurationWidth'] = getMinDurationWidth(
    minEndingIdx,
    maxEndingIdx,
    startingIdx
  )

  return style
}

/**
 * Get min-duration width in percentage
 */
export function getMinDurationWidth(
  minEndingIdx: number,
  maxEndingIdx: number,
  startingIdx: number
): string {
  return (
    (
      (100 * Math.max(1, minEndingIdx - startingIdx)) /
      Math.max(1, maxEndingIdx - startingIdx)
    ).toPrecision(3) + '%'
  )
}

/**
 * Returns starting date for selected element
 */
export function positionToMoment(
  event: { x: number },
  target: HTMLElement,
  start = true
): Moment {
  const targetBox = target.getBoundingClientRect()
  const idx =
    Math.floor(
      (event.x - draggedObject.offset - targetBox.x) / config.blockWidth
    ) + (start ? 0 : 1)
  // Idx must be in array: 0 ≤ idx ≤ Last index
  return blocks.value[Math.min(Math.max(idx, 0), blocks.value.length - 1)]
}

/**
 * Return style CSS variables for width of timetable
 */
export function timelineWidth(): string {
  return `--timeline-width: ${config.blockWidth * blocks.value.length}px`
}

type OutOfBandResult = {
  startBefore: ComputedRef<boolean>
  endAfter: ComputedRef<boolean>
}

export function handleOutOfBound(
  element: Ref<{
    startDate?: Date | null
    estimatedEndMax?: Date
  }>
): OutOfBandResult {
  return {
    startBefore: computed(
      () =>
        !!element.value.startDate &&
        minDate.value.isAfter(element.value.startDate, 'minute')
    ),
    endAfter: computed(() =>
      maxDate.value.isBefore(element.value.estimatedEndMax)
    ),
  }
}
