import moment from 'moment-timezone'
import {
  Config,
  config,
  DEFAULT_CONFIG,
  ROW_OPTIONS,
  RowOptions,
} from '@/states/weekPlanning'
import {
  LocationQuery,
  LocationQueryRaw,
  onBeforeRouteUpdate,
  RouteLocationNormalized,
  useRoute,
  useRouter,
} from 'vue-router'
import { toRaw, watch } from 'vue'
import { getChanges } from '@/components/form/PlainRecorder'
import type { Workforce } from '@/api'
import { isEqual } from 'lodash'

const DATE_FORMAT = 'YYYY-MM-DD'

type configNumberFields = 'blockWidth' | 'hoursPerBlock' | 'nbDays'

function addIfNumberAndDifferentFromConfig(
  query: LocationQuery,
  field: configNumberFields,
  min: number
) {
  const paramAsNumber = Number(query[field]) as unknown as number
  if (Number.isNaN(paramAsNumber)) return
  if (paramAsNumber >= min && config[field] !== paramAsNumber)
    config[field] = paramAsNumber
}

/**
 * Extract selected workforces from query params & set week-planning configuration
 */
function addSelectedWorkforces(query: LocationQuery): void {
  if (
    query.selectedWorkforces &&
    !isEqual(query.selectedWorkforces, toRaw(config.selectedWorkforces))
  ) {
    let selected = query.selectedWorkforces

    if (!Array.isArray(selected)) {
      selected = [selected]
    }

    // Make sure all items are valid
    config.selectedWorkforces = selected.filter(
      (item) => typeof item === 'string'
    ) as Array<Workforce['id']>
  }
}

function updateConfigFromRouteQuery(route: RouteLocationNormalized) {
  const query = route.query
  const current = moment(String(route.query.date), DATE_FORMAT, true)
  if (current.isValid() && !current.isSame(moment(config.date), 'day'))
    config.date = current.toDate()

  if (ROW_OPTIONS.includes(query.row as RowOptions)) {
    const paramAsRowOptions = query.row as RowOptions
    if (config.row !== paramAsRowOptions) config.row = paramAsRowOptions
  }

  addSelectedWorkforces(query)
  addIfNumberAndDifferentFromConfig(route.query, 'hoursPerBlock', 0.25)
  addIfNumberAndDifferentFromConfig(route.query, 'nbDays', 1)
  addIfNumberAndDifferentFromConfig(route.query, 'blockWidth', 4)

  if (route.query.resourceAsTitle) config.resourceAsTitle = true
}

export function useRouteQuery(): void {
  const currentRoute = useRoute()
  updateConfigFromRouteQuery(currentRoute)
  onBeforeRouteUpdate(updateConfigFromRouteQuery)

  const router = useRouter()
  watch(
    () => config,
    () => {
      const changes: Partial<Config> = getChanges(DEFAULT_CONFIG, toRaw(config))
      const query: LocationQueryRaw = changes as LocationQueryRaw
      if (query.date) query.date = moment(config.date).format(DATE_FORMAT)
      router.replace({ ...currentRoute, query })
    },
    { deep: true }
  )
}
