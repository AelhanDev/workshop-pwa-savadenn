import { computed, Ref, ref, watchEffect } from 'vue'
import { Task } from '@/api'
import {
  ProvidedResources,
  ProvidedWeekPlanning,
  ResourcesKey,
  WeekPlanningKey,
} from '@/provided'
import { injectStrict } from '@/injectStrict'

export type UseTaskLabel = {
  label: Ref<string>
  code: Ref<string | undefined>
}

export function useTaskLabel(task: Ref<Task>): UseTaskLabel {
  const label = ref('')

  const Resources = injectStrict<ProvidedResources>(ResourcesKey)
  const WeekPlanning = injectStrict<ProvidedWeekPlanning>(WeekPlanningKey)

  const businessCase = Resources.getCachedBusinessCase(task.value.businessCase)

  watchEffect(async () => {
    if (WeekPlanning.config.resourceAsTitle) {
      if (WeekPlanning.config.row === 'byBusinessCase') {
        label.value = task.value.assignedTo
          ? await Resources.getWorkforceName(task.value.assignedTo)
          : task.value.title
        return
      }

      if (WeekPlanning.config.row === 'byWorkforce') {
        label.value =
          Resources.getCachedBusinessCase(task.value.businessCase).value.name ||
          '-'
        return
      }
    }
    label.value = task.value.title
  })

  return {
    label,
    code: computed(() => businessCase.value.code),
  }
}
