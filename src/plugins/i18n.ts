import { createI18n } from 'vue-i18n'
import * as moment from 'moment-timezone'
import 'moment/dist/locale/fr'
import 'moment/dist/locale/en-gb'
import { settings } from '@/states'
import { watchEffect } from 'vue'

/*
 * All i18n resources specified in the plugin `include` option can be loaded
 * at once using the import syntax
 */
import messages from '@intlify/vite-plugin-vue-i18n/messages'

export const i18n = createI18n({
  fallbackLocale: 'en',
  locale: settings.lang,
  messages,
  legacy: false,
})

const CODES: Record<string, string> = {
  en: 'en-gb',
  fr: 'fr',
}

watchEffect(() => {
  i18n.global.locale.value = settings.lang
  moment.locale(CODES[settings.lang])
})
