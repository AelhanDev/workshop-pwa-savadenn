/* istanbul ignore file */
import {
  Home,
  Settings,
  BusinessCases,
  WeekPlanning,
  Task,
  Teams,
  Machines,
  BusinessCase,
} from '@/pages'
import {
  createRouter,
  createWebHashHistory,
  RouteLocationNormalized,
  RouteLocationRaw,
  RouteRecordRaw,
  RouterOptions,
} from 'vue-router'
import { getType, ResourceType, userClient } from '@/api'
import { addMessage, updateCurrentUser } from '@/states'
import { i18n } from '@/plugins/i18n'
import { NamedRoute, ROUTES } from '@/plugins/routes'

const routes: RouteRecordRaw[] = [
  <RouteRecordRaw>{
    ...ROUTES.HOME,
    path: '/home',
    component: Home,
  },
  <RouteRecordRaw>{
    ...ROUTES.SETTINGS,
    path: '/settings',
    component: Settings,
  },
  <RouteRecordRaw>{
    ...ROUTES.BUSINESS_CASE,
    path: '/business-cases/:uuid',
    component: BusinessCase,
    props: true,
  },
  <RouteRecordRaw>{
    ...ROUTES.BUSINESS_CASES,
    path: '/business-cases',
    component: BusinessCases,
  },
  <RouteRecordRaw>{
    ...ROUTES.TASK,
    path: '/tasks/:uuid',
    component: Task,
    props: true,
  },
  <RouteRecordRaw>{
    ...ROUTES.WEEK_PLANNING,
    path: '/week-planning',
    component: WeekPlanning,
  },
  <RouteRecordRaw>{
    ...ROUTES.MACHINES,
    path: '/machines',
    component: Machines,
  },
  <RouteRecordRaw>{
    ...ROUTES.TEAMS,
    path: '/teams',
    component: Teams,
  },
  <RouteRecordRaw>{
    ...ROUTES.RESOURCE,
    path: '/resource/:uuid',
    redirect: redirectToResource,
  },
  <RouteRecordRaw>{
    path: '/:pathMatch(.*)*',
    redirect: '/home',
  },
]

export const Router = createRouter(<RouterOptions>{
  history: createWebHashHistory(),
  routes,
})

// https://next.router.vuejs.org/guide/advanced/navigation-guards.html#navigation-guards
Router.beforeEach((to) => {
  if (handleConfirmEmail(to)) return '/'
})

/**
 * Consume token for an email confirmation change if url matches, return false otherwise
 */
function handleConfirmEmail(to: RouteLocationNormalized): boolean {
  const token = to.query['confirm_email_token']
  if (!token) return false

  userClient
    .consumeConfirmEmailToken(token as string)
    .then(() => {
      addMessage({
        severity: 'success',
        summary: i18n.global.t('toast.success.change_email.confirmed.summary'),
        detail: i18n.global.t('toast.success.change_email.confirmed.detail'),
        life: 6000,
      })
    })
    .then(() => updateCurrentUser())

  return true
}

/**
 * Resource route mapping
 */
const RESOURCE_ROUTING: Record<ResourceType, NamedRoute | undefined> = {
  [ResourceType.User]: undefined,
  [ResourceType.Task]: ROUTES.TASK,
  [ResourceType.BusinessCase]: ROUTES.BUSINESS_CASE,
  [ResourceType.team]: undefined,
  [ResourceType.machine]: undefined,
  [ResourceType.workforce]: undefined,
}

/**
 * Redirect to given resource page
 */
function redirectToResource(to: RouteLocationNormalized): RouteLocationRaw {
  const id = to.params.uuid as string
  const type = getType({ id })
  if (!type || !RESOURCE_ROUTING[type]) return ROUTES.HOME
  return { ...RESOURCE_ROUTING[type], params: { uuid: id } }
}
