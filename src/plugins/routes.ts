export type NamedRoute = { name: string }

export const ROUTES: Record<string, NamedRoute> = {
  HOME: { name: 'page.home.title' },
  SETTINGS: { name: 'page.settings.title' },
  BUSINESS_CASE: { name: 'page.business_case.title' },
  BUSINESS_CASES: { name: 'page.business_cases.title' },
  MACHINES: { name: 'page.machines.title' },
  RESOURCE: { name: 'page.resource.title' },
  TASK: { name: 'page.task.title' },
  TEAMS: { name: 'page.teams.title' },
  WEEK_PLANNING: { name: 'page.weekPlanning.title' },
  WORKFORCES: { name: 'page.workforces.title' },
}
