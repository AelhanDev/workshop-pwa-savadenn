import { Now, Resources, settings, WeekPlanning } from '@/states'
import { InjectionKey } from 'vue'

export const ResourcesKey: InjectionKey<typeof Resources> = Symbol('Resources')
export const NowKey: InjectionKey<typeof Now> = Symbol('Now')
export const SettingsKey: InjectionKey<typeof settings> = Symbol('Settings')
export const WeekPlanningKey: InjectionKey<typeof WeekPlanning> =
  Symbol('WeekPlanning')

export type ProvidedResources = typeof Resources
export type ProvidedNow = typeof Now
export type ProvidedSettings = typeof settings
export type ProvidedWeekPlanning = typeof WeekPlanning
