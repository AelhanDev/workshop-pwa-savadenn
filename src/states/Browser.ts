import * as Types from '@/types'
import moment from 'moment-timezone'

/**
 * Get the timeZone of the browser
 */
export function getTimeZone(): string {
  return moment.tz.guess(true)
}

/**
 * Get lang of the browser
 */
export function getLang(): string {
  return window.navigator.language.substr(0, 2)
}

/**
 * Get theme of browser
 */
export function getTheme(): Types.Theme {
  return matchMedia('(prefers-color-scheme: dark)').matches
    ? 'themeDark'
    : 'themeLight'
}
