import { ref } from 'vue'

type MessageSeverity = 'success' | 'warn' | 'info' | 'error'

export type Message = {
  severity?: MessageSeverity
  summary?: string
  detail?: string
  life?: number
  closable?: boolean
  group?: string
}

/**
 * List of messages to display as toast
 */
export const messages = ref<Message[]>([])

/**
 * Adds a new message to display
 */
export function addMessage(message: Message): void {
  messages.value.push(message)
}
