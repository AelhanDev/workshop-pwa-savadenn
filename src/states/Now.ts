import moment from 'moment-timezone'
import { ref, watch } from 'vue'
import { settings } from '@/states/Settings'

export const Now = ref(moment())

function updateNow() {
  Now.value = moment().tz(settings.timeZone)
}

watch(() => settings.timeZone, updateNow)

export function startTime(): void {
  updateNow()
  window.setTimeout(startTime, 250)
}
