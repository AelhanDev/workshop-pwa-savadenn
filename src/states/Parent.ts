import { getApiClient, getParentId as getPId, Resource } from '@/api'

/**
 * ParentID, false means no parent
 */
type ParentId = Resource['id'] | false

const bufferParents = new Map<Resource['id'], Promise<ParentId>>()

/**
 * Use API call to find parentId, cache it
 */
async function getFreshParentId(id: Resource['id']): Promise<ParentId> {
  const client = await getApiClient()
  const { data } = await client.get(id)
  const parentId = getPId({ ...data, id: data['@id'] })
  return parentId || false
}

/**
 * Returns parentId if any
 */
export async function getParentId(id: Resource['id']): Promise<ParentId> {
  let promiseId = bufferParents.get(id)
  if (!promiseId) {
    promiseId = getFreshParentId(id)
    bufferParents.set(id, promiseId)
  }
  return promiseId
}
