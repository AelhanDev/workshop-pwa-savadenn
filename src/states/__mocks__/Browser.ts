import * as Types from '@/types'

/**
 * Mock for Browser.ts as jest does not have same HTML API
 */

/**
 * Get the timeZone of the browser
 */
export function getTimeZone(): string {
  return 'Europe/Paris'
}

/**
 * Get lang of the browser
 */
export function getLang(): string {
  return 'en'
}

/**
 * Get theme of browser
 */
export function getTheme(): Types.Theme {
  return 'themeLight'
}
