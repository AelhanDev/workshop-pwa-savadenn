import { updateShortName, getShortName } from '@/states/ShortNames'
import MockAdapter from 'axios-mock-adapter'
import axios from 'axios'
import { nextTick } from 'vue'
import { Resource } from '@/api'

const mockAxios = new MockAdapter(axios, { onNoMatch: 'throwException' })
jest.mock('@/api/Client')

describe('shortName', () => {
  test('Unknown resource', async () => {
    mockAxios.onGet('/api/nonMapped/foo').reply(200, {
      '@id': '/api/nonMapped/foo',
    })
    const shortName = getShortName('/api/nonMapped/foo')
    expect(shortName.value).toEqual('-')
    await nextTick()
    expect(shortName.value).toEqual('-')
  })

  test('Report only error in API', async () => {
    mockAxios.onGet('/api/notfound/foo').reply(404)
    const shortName = getShortName('/api/notfound/foo')
    expect(shortName.value).toEqual('-')
    await nextTick()
    expect(shortName.value).toEqual('-')
  })

  test('Task', async () => {
    const test = { id: '/api/tasks/12345', title: 'foo' } as Resource
    updateShortName(test)
    await nextTick()
    expect(getShortName(test.id).value).toEqual('foo')
  })

  test('BusinessCase', async () => {
    const test = {
      id: '/api/business_cases/12345',
      name: 'foo',
      code: '123',
    } as Resource
    updateShortName(test)
    await nextTick()
    expect(getShortName(test.id).value).toEqual('foo #123')
  })
})
