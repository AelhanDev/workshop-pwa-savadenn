export * as Browser from './Browser'
export * from './Settings'
export * from './Now'
export * as Resources from './resources'
export * as WeekPlanning from './weekPlanning'
export * from './Messages'

import { startTime } from './Now'
import { loadSavedSettings } from './Settings'

/**
 * Initialize states asynchronously
 */
export async function initialize(): Promise<void[]> {
  startTime()
  return Promise.all([loadSavedSettings()])
}
