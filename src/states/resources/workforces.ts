import { computed, ref, watch } from 'vue'
import { ResourceMap, Workforce, workforceClient } from '@/api'
import { settings } from '@/states'

const workforces = ref<ResourceMap<Workforce>>(
  new Map<Workforce['id'], Workforce>()
)

export async function loadWorkforces() {
  if (settings.user.id) {
    workforces.value.clear()
    const { resources } = await workforceClient.getCollection({})
    for (const workforce of resources) {
      workforces.value.set(workforce.id, workforce)
    }
  }
}

export async function initWorkforces(): Promise<void> {
  await loadWorkforces()
  watch(() => settings.user.id, loadWorkforces)
}

/**
 * All workforces Id
 */
export const allId = computed(() => [...workforces.value.keys()])

/**
 * All workforces
 */
export const allWorkforces = computed(() => [...workforces.value.values()])

/**
 * Get Workforce name with local cache
 */
export async function getWorkforceName(id: string): Promise<string> {
  let workforce = workforces.value.get(id)
  if (!workforce) {
    workforce = await workforceClient.getItem(id)
    workforces.value.set(workforce.id, workforce)
  }
  return workforce.name
}
