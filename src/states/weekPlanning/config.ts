import { BY_WORK_FORCE, RowOptions } from '.'
import { computed, reactive } from 'vue'
import { cloneDeep } from 'lodash'
import { Workforce } from '@/api'

export type Config = {
  row: RowOptions

  /**
   * Anchor date of planning view, null date means center on relative today
   */
  date: Date | null

  hoursPerBlock: number
  nbDays: number
  blockWidth: number
  resourceAsTitle: boolean

  /**
   * List of workforces for which to display tasks
   */
  selectedWorkforces: Array<Workforce['id']>
}

export const DEFAULT_CONFIG: Config = {
  row: BY_WORK_FORCE,
  date: null,
  hoursPerBlock: 2,
  nbDays: 7,
  blockWidth: 40,
  resourceAsTitle: false,
  selectedWorkforces: [],
}

export const config = reactive<Config>(cloneDeep(DEFAULT_CONFIG))

export const referenceDate = computed(() => config.date || new Date())
