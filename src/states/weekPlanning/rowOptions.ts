export const BY_BUSINESS_CASE = 'byBusinessCase'
export const BY_WORK_FORCE = 'byWorkforce'

export type RowOptions = typeof BY_BUSINESS_CASE | typeof BY_WORK_FORCE

export const ROW_OPTIONS: RowOptions[] = [BY_BUSINESS_CASE, BY_WORK_FORCE]
