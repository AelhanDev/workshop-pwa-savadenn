export type ListOptions<T = unknown> = { label: string; value: T }[]
