import { JwtPayload as Base } from 'jwt-decode'

export type JwtPayload = Base & {
  guid: string
}
