import { FixturesProfile } from '../utils/fixturesProfile'
import { getJwtsStub } from '../utils/jwtProvider'

export const fixturesProfile: FixturesProfile = {
  '/auth/authentication_token': getJwtsStub(),
  '/auth/refresh_token': getJwtsStub(),
  '/auth/users/1': {
    fixture: 'api/users/user.json',
  },
}
