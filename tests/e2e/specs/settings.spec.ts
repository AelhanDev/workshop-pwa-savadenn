import { fixturesProfile } from './fixturesProfile'
import { stubAll } from '../utils/stubAll'
import { logIn, logOut } from '../utils/logging'

describe('Settings', () => {
  beforeEach(() => {
    cy.intercept(Cypress.env('API_ENDPOINT'), stubAll(fixturesProfile))
    logIn()
  })

  afterEach(() => {
    logOut()
  })

  it('Access profile via menu', () => {
    cy.visit('/')
    cy.get('h1').should('have.text', 'Home')
    cy.get('.TopBar__AccountMenu').click()
    cy.get('.Menu__Settings').should('have.text', 'Settings').click()
    cy.get('h1').should('have.text', 'Settings')
  })

  it('should modify personal data', () => {
    cy.visit('/#/settings')
    cy.get('.Profile__Dataname')
      .should('have.value', 'Rick Sanchez')
      .clear()
      .should('have.value', '')
      .type('Pickle Rick')

    cy.intercept(
      Cypress.env('API_ENDPOINT'),
      stubAll({
        ...fixturesProfile,
        '/auth/users/1': {
          fixture: 'api/users/user-profileUpdated.json',
        },
      })
    )

    cy.get('.p-toast-summary')
      .should('be.visible')
      .should('have.text', 'Updated')

    cy.get('.p-toast-detail')
      .should('be.visible')
      .should('have.text', 'Profile')

    cy.get('.TopBar__AccountMenu .p-button-label').should(
      'have.text',
      'Pickle Rick'
    )
  })

  it('Change language', () => {
    cy.visit('/#/settings')
    cy.get('h1').should('have.text', 'Settings')

    cy.get('.Settings__Language')
      .find('.p-dropdown-label')
      .should('have.text', 'English')
    cy.get('.Settings__Language').click()

    cy.get('.p-dropdown-item[aria-label="Français"]').click()

    cy.get('h1').should('have.text', 'Paramètres')
  })

  it('Change theme', () => {
    cy.visit('/#/settings')

    cy.get('body').should('have.class', 'Body--themeLight')

    cy.get('.Settings__Theme')
      .find('.p-dropdown-label')
      .should('have.text', 'Light')

    cy.get('.Settings__Theme').click()

    cy.get('.p-dropdown-item[aria-label="Dark"]').click()

    cy.get('body').should('have.class', 'Body--themeDark')
  })
})
