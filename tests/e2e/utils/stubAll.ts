import { HttpRequestInterceptor } from 'cypress/types/net-stubbing'
import { FixturesProfile } from './fixturesProfile'
import { stubRest } from './stubRest'
import { stubGraphQL } from './stubGraphQL'

/**
 * Stubs all distant calls with profiled fixtures
 * @param fixtureProfile
 */
export const stubAll =
  (fixtureProfile: FixturesProfile): HttpRequestInterceptor =>
  async (req) => {
    if (new URL(req.url).pathname.match(/^\/graphql/)) {
      return stubGraphQL(fixtureProfile)(req)
    }

    return stubRest(fixtureProfile)(req)
  }
