import { HttpRequestInterceptor } from 'cypress/types/net-stubbing'
import { FixturesProfile } from './fixturesProfile'

/**
 * Get operation name from request `query operationName {`
 */
const OPERATION_NAME_REGEX = /(?<=^query\s*)\w+(?=\s*\(|{)/

/**
 * Stubs the calls to graphql with profiled fixtures
 * @param fixtureProfile
 */
export const stubGraphQL =
  (fixtureProfile: FixturesProfile): HttpRequestInterceptor =>
  async (req) => {
    const matching = req.body.query.match(OPERATION_NAME_REGEX)
    const operationName = matching[0]
    const operationFixture = fixtureProfile[operationName]
    if (operationFixture) {
      console.log('Stubbing GraphQL operation', operationFixture)
      req.alias = `gql${operationName}`
      req.reply({ statusCode: 200, ...operationFixture })
    } else {
      console.warn('Missing GraphQL mock')
      req.reply({ statusCode: 404, body: 'Missing GraphQL mock' })
    }
  }
