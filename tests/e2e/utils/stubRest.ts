import { HttpRequestInterceptor } from 'cypress/types/net-stubbing'
import { FixturesProfile } from './fixturesProfile'

/**
 * Stubs the calls to API with profiled fixtures
 * @param fixtureProfile
 */
export const stubRest =
  (fixtureProfile: FixturesProfile): HttpRequestInterceptor =>
  async (req) => {
    const url = new URL(req.url)
    const operationFixture = fixtureProfile[url.pathname]
    if (operationFixture) {
      console.log('Stubbing API call', operationFixture)
      req.reply({ statusCode: 200, ...operationFixture })
    } else {
      console.warn(`Missing API mock for ${url.pathname}`)
      req.reply({ statusCode: 404, body: 'Missing API mock' })
    }
  }
